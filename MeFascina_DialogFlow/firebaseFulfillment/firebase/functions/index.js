// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';
const nodemailer = require('nodemailer');
const http = require('http');
const functions = require('firebase-functions');
const host = 'dinojohny-eval-prod.apigee.net';
let registeredUsers =['neveffects@gmail.com','nevin@litmus7.com','sephoravoice@gmail.com'];

exports.dialogflowFirebaseFulfillment = functions.https.onRequest((req, res) => {
	let action = req.body.queryResult.action;
    if (action== 'actions.intent.MAIN'){
    	console.log('YOU ARE IN MAIN INTENT');    
    	if(registeredUsers.includes('sephoravoice@gmail.com')){
    		console.log('YOU ARE A registered USER ');    
    		let username = 'Meaghan';
		    // Call the Login API
		  callLoginApi(username).then((output) => {
		    res.json({ 'fulfillmentText': output }); // Return the results of
														// the
														// Login API to
														// Dialogflow
		  }).catch(() => {
		    res.json({ 'fulfillmentText': `Something went wrong!!` });});
		  }else{
			    let output='YOU ARE NOT A registered USER ';
			    console.log(output);  
			    return new Promise((resolve, reject) => {
			    resolve(output);});
		  }
    	}else if(action=='actions.intent.Checkout'){
    		callCheckoutApi(username).then((output) => {
    			res.json({ 'fulfillmentText': output }); // Return the
															// results of the
												// Checkout API to Dialogflow
    		}).catch(() => {
    			res.json({ 'fulfillmentText': `Something went wrong!!` });});
    	}else if(action=='actions.intent.BIPoints'){
    		callGetBIPointsApi(username).then((output) => {
            res.json({ 'fulfillmentText': output }); // Return the results of
														// the
        												// BIPoints API to
														// Dialogflow
         }).catch(() => {
        	 res.json({ 'fulfillmentText': `Something went wrong!!` });});
    	}		
});


function callLoginApi (username) {
	//Mail Sending Functionality	
	let transporter = nodemailer.createTransport({
        service: 'gmail',
        host: 'smtp.googlemail.com',
        port: 587,
        starttls: true,
        auth: {
            user: 'abc@gmail.com', // generated ethereal user
            pass: 'pass' // generated ethereal password
        }
    });
    
       let mailOptions = {
        from: '"MeFascina" <mefascina@gmail.com>', // sender address
        to: 'abc@gmail.com', // list of receivers
        subject: 'Order Placed!', // Subject line
        text: 'Your Order Placed Successfully!', // plain text body
        html: '<b>Your Order Placed Successfully!</b>' // html body
    };
   
       // send mail with defined transport object
	    transporter.sendMail(mailOptions, (error, info) => {
	        if (error) {
	            return console.log(error);
	        }
	        console.log('Message sent: %s', info.messageId);
	        // Preview only available when sending through an Ethereal account
	        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
	
	        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
	        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
	    });
    
	  return new Promise((resolve, reject) => {
	    // Create the path for the HTTP request to get the Login Details
	    let path = '/dracathon/login?loginName='+encodeURIComponent(username);
	    console.log('API Request: ' + host + path);
	
	    // Make the HTTP request to get the login details.
	    http.get({host: host, path: path}, (res) => {
	      let body = ''; // var to store the response chunks
	      res.on('data', (d) => { body += d; }); // store each response chunk
	      res.on('end', () => {
	        // After all the data has been received parse the JSON for desired data
	        let response = JSON.parse(body);
	        let profile = response['data']['profile'][0];
	        
	        // Create response
	        let output = `Welcome  ${profile['name']}. How may I assist you?`;
	
	        // Resolve the promise with the output text
	        console.log(output);
	        resolve(output);
	      });
	      res.on('error', (error) => {
	        console.log(`Error calling login API: ${error}`)
	        reject();
	      });
	    });
	  });
	}

function callCheckoutApi (username) {
  return new Promise((resolve, reject) => {
    // Create the path for the HTTP request to get the cart details
    let path = '/dracathon/cart?loginName='+encodeURIComponent(username);
    console.log('API Request: ' + host + path);

    // Make the HTTP request to get the cart details
    http.get({host: host, path: path}, (res) => {
      let body = ''; // var to store the response chunks
      res.on('data', (d) => { body += d; }); // store each response chunk
      res.on('end', () => {
        // After all the data has been received parse the JSON for desired data
        let response = JSON.parse(body);
        let cart1 = response['data']['Cart'][0];
        let cart2 = response['data']['Cart'][1];
        let cart3 = response['data']['Cart'][2];

        // Create response
        let output = `Ok! Checkout initiated with the following products : ${cart1['Name']} and ${cart2['Name']}. 
        You will receive an email soon with a confirmation. Thank You for shopping!`;
        
        // Resolve the promise with the output text
        console.log(output);
        resolve(output);
      });
      res.on('error', (error) => {
        console.log(`Error calling login API: ${error}`)
        reject();
      });
    });
  });
}

function callGetBIPointsApi (username) {
  return new Promise((resolve, reject) => {
    // Create the path for the HTTP request to get the BI Point
    let path = '/dracathon/biPoints?biPoints=1500';
    console.log('API Request: ' + host + path);

    // Make the HTTP request to get the BI Point
    http.get({host: host, path: path}, (res) => {
      let body = ''; // var to store the response chunks
      res.on('data', (d) => { body += d; }); // store each response chunk
      res.on('end', () => {
        // After all the data has been received parse the JSON for desired data
        let response = JSON.parse(body);
        let profile = response['data']['profile'][0];

        // Create response
        let output = `Hey Meghan you have  ${profile['biPoints']} points!`;

        // Resolve the promise with the output text
        console.log(output);
        resolve(output);
      });
      res.on('error', (error) => {
        console.log(`Error calling login API: ${error}`)
        reject();
      });
    });
  });
}