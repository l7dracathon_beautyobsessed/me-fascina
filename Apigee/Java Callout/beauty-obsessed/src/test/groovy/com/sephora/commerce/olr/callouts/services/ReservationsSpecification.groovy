package com.sephora.commerce.olr.callouts.services;

import java.util.HashMap;

import com.apigee.flow.execution.ExecutionContext
import com.apigee.flow.message.MessageContext
import com.apigee.flow.message.Message

import com.sephora.commerce.olr.callouts.common.OLRConstants

import spock.lang.Ignore
import spock.lang.Specification

class ReservationsSpecification extends Specification {

	private Map map = new HashMap();
	private String response;
	public boolean setVariable(String name, Object value) {
		map.put(name, value);
	}

	public <T> T getVariable(String key) {
		return map.get(key);
	}
	
	/**
	 * Integration test for get activities
	 * add @Ignore annotation if dont want to run it.
	 * @return
	 */
	def "Get Reservations Test URI:/v1/olr/users/profiles/{clientId}/reservations"() {
		
		given:
		ReservationsService service = new ReservationsService();
		ExecutionContext executionContext = Mock()
		def context = Mock(MessageContext)
		context.getVariable("sdn.vaq.router.host") >> "vaq-r-dev-sdn.internalsephora.com"
		context.getVariable("sdn.tew.router.host") >> "tew-r-dev-sdn.internalsephora.com"
		context.getVariable(OLRConstants.VARIABLE_ENABLE_LOGGING) >> "true"
		context.getVariable(OLRConstants.VARIABLE_PF_MONITOR) >> "true"
		context.getVariable("environment.name") >> "dev"
		context.getVariable("proxy.url") >> "https://vaq-r-dev-sdn.internalsephora.com"
		context.getVariable(OLRConstants.VAR_EC_ACCESS_CLIENT_ID) >> "3c075100-5071-42ee-b040-afebae231ad5"
		context.getVariable(OLRConstants.VAR_EC_ACCESS_CLIENT_SECRET) >> "2c4f751380850624db60990b99049849b28db68094b51759794e80a03518315c4aad9081705c777f1e2927c3515dcc44e004900cdf4a656a0f96773b5367e11"
		context.getVariable(OLRConstants.URI_PATH_EMAIL_ID) >> "nebu@test.com"
		context.getVariable(OLRConstants.ENGAGEMENT_CENTER_ENABLED) >> "true"
		context.getVariable(OLRConstants.VAR_PAST_RESERVE_FETCH_IN_MONTHS) >> "12"
		context.getVariable(OLRConstants.VAR_FUTURE_RESERVE_FETCH_IN_MONTHS) >> "3"		
		context.getVariable(_) >> { String key -> getVariable(key)}
		context.setVariable(_, _) >> { String key, Object value -> setVariable(key, value)}
		context.getMessage() >> Mock(Message)
		context.getMessage().setContent(_) >> { String response -> this.response = response}


		when:
		service.execute(context, executionContext)

		then:
		println "\n****************Response**Start***************"
		println context.getVariable(OLRConstants.VAR_TTS_RESPONSE_MESSAGE)
		println response
		println "\n****************Response**End*****************"
	}
}
