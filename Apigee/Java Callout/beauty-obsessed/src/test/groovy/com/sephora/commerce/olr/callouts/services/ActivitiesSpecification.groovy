package com.sephora.commerce.olr.callouts.services;

import static com.sephora.commerce.olr.callouts.common.OLRConstants.*

import com.apigee.flow.execution.ExecutionContext
import com.apigee.flow.message.MessageContext
import com.apigee.flow.message.Message;

import spock.lang.Ignore
import spock.lang.Specification
import spock.util.environment.RestoreSystemProperties

class ActivitiesSpecification extends Specification {

	private Map map = new HashMap();
	private String response;
	
	public boolean setVariable(String name, Object value) {
		map.put(name, value);
	}

	public <T> T getVariable(String key) {
		return map.get(key);
	}
	/**
	 * Integration test for get activities
	 * add @Ignore annotation if dont want to run it.
	 * @return
	 */
	@RestoreSystemProperties
	def "Get activities Integration Test URI:/v1/olr/activities?storeIds=0732,0058"() {
		
		given:
		System.setProperty("olrConfigFile",'C:\\Users\\Jitendra\\Desktop\\OlrConfig.properties')
		ActivitiesService service = new ActivitiesService();
		def context = Mock(MessageContext)
		context.getVariable("sdn.vaq.router.host") >> "vaq-r-dev-sdn.internalsephora.com"
		context.getVariable("sdn.tew.router.host") >> "tew-r-dev-sdn.internalsephora.com"
		context.getVariable("environment.name") >> "dev"
		context.getVariable("proxy.url") >> "https://vaq-r-dev-sdn.internalsephora.com"
		context.getVariable(VARIABLE_ENABLE_LOGGING) >> "true"
		context.getVariable(USE_CACHE) >> "true"
		context.getVariable(VARIABLE_PF_MONITOR) >> "true"
		context.getVariable(ENGAGEMENT_CENTER_ENABLED) >> "true"
		//context.getVariable(MESSAGE_QUERYPARAM_STARTDATE) >> "2018-08-21T00:00:00Z"
		//context.getVariable(MESSAGE_QUERYPARAM_ENDDATE) >> "2018-09-31T00:00:00Z"
		context.getVariable(EC_STORE_LIST) >> "0600|0058"
		context.getVariable(COORD_PROGRAM_TYPES) >> "classes|events"
		context.getVariable(MESSAGE_QUERYPARAM_CAMPAIGN_ID) >> ""
		context.getVariable(SHARED_PROGRAM_TYPE) >> "services"
		context.getVariable(MESSAGE_QUERYPARAM_STORE_IDS) >> "0600,0058,0202,0382"
		context.getVariable(MESSAGE_QUERYPARAM_PROGRAM_TYPES) >> "classes,events"
		context.getVariable(VAR_EC_ACCESS_CLIENT_ID) >> "3c075100-5071-42ee-b040-afebae231ad5"
		context.getVariable(VAR_EC_ACCESS_CLIENT_SECRET) >> "2c4f751380850624db60990b99049849b28db68094b51759794e80a03518315c4aad9081705c777f1e2927c3515dcc44e004900cdf4a656a0f96773b5367e11"
		context.getVariable(_) >> { String key -> getVariable(key)}
		context.setVariable(_, _) >> { String key, Object value -> setVariable(key, value)}
		context.getMessage() >> Mock(Message)
		context.getMessage().setContent(_) >> { String response -> this.response = response}


		when:
		4.times({
			service.execute(context, Mock(ExecutionContext))
		})
		

		then:
		println "\n****************Response**Start***************"
		println response
		println "\n****************Response**End*****************"
		response != null
	}
}
