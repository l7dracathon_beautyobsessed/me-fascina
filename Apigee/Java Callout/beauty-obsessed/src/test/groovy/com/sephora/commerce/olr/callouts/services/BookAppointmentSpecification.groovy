package com.sephora.commerce.olr.callouts.services;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.HashMap;

import com.apigee.flow.execution.ExecutionContext
import com.apigee.flow.message.MessageContext
import com.apigee.flow.message.Message

import com.sephora.commerce.olr.callouts.common.OLRConstants
import com.sephora.commerce.olr.callouts.model.response.OLRErrorResponse;

import spock.lang.Ignore
import spock.lang.Specification

class BookAppointmentSpecification extends Specification {

	private Map map = new HashMap();
	private String inputJsonStringCD = "{\r\n" + 
				"	\"activityType\":\"classes\",\r\n" + 
				"	\"bookingId\":\"OLR-CD__contouring_class\",\r\n" + 
				"	\"channelId\":\"web\",\r\n" +
				"	\"storeId\":\"0058\",\r\n" + 
				"	\"startDateTime\":\"2018-09-20T19:00:00Z\",\r\n" + 
				"	\"clientExternalId\":\"nebu@litmus7.com\",\r\n" + 
				"	\"firstName\":\"Nebu\",\r\n" + 
				"	\"lastName\":\"Joseph\",\r\n" + 
				"	\"phone\":\"6506276144\",\r\n" + 
				"	\"smsEnabled\":true\r\n" + 
				"}";
		private String inputJsonStringEC = "{\r\n" + 
				"	\"activityType\":\"classes\",\r\n" + 
				"	\"bookingId\":\"OLR-EC__GrHLXQx9\",\r\n" + 
				"	\"channelId\":\"web\",\r\n" +
				"	\"storeId\":\"0202\",\r\n" + 
				"	\"startDateTime\":\"2018-09-20T21:00:00Z\",\r\n" + 
				"	\"clientExternalId\":\"nebu@litmus7.com\",\r\n" + 
				"	\"firstName\":\"Nebu\",\r\n" + 
				"	\"lastName\":\"Joseph\",\r\n" + 
				"	\"phone\":\"6506276144\",\r\n" + 
				"	\"smsEnabled\":true\r\n" + 
				"}";			
	private String response;
	public boolean setVariable(String name, Object value) {
		map.put(name, value);
	}

	public <T> T getVariable(String key) {
		return map.get(key);
	}
	
/*	public void setContext(Mock context, String sourceSystem, String eventId) {
		context.getVariable("sdn.vaq.router.host") >> "vaq-r-dev-sdn.internalsephora.com";
		context.getVariable("sdn.tew.router.host") >> "tew-r-dev-sdn.internalsephora.com";
		context.getVariable("environment.name") >> "dev";
		context.getVariable("proxy.url") >> "https://vaq-r-dev-sdn.internalsephora.com";
		context.getVariable("defaultSourceSystem") >> sourceSystem;
		context.getVariable(OLRConstants.URI_PATH_ACTIVITY_ID) >> eventId;
	}
		*/
		
	/**
	 * Integration test for book appointment for EC system
	 * add @Ignore annotation if dont want to run it.
	 * @return
	 */
	def "Post Book Appointment EC Test URI:/v1/olr/activities/{activityId}"() {
		
		given:
		BookAppointmentService  service= new BookAppointmentService();
		ObjectMapper objectMapper = new ObjectMapper();
		ExecutionContext executionContext = Mock()
		def context = Mock(MessageContext)
		service.setLogDebugEnabled(true);
		context.getVariable("sdn.vaq.router.host") >> "vaq-r-dev-sdn.internalsephora.com"
		context.getVariable("sdn.tew.router.host") >> "tew-r-dev-sdn.internalsephora.com"
		context.getVariable("environment.name") >> "dev"
		context.getVariable("proxy.url") >> "https://vaq-r-dev-sdn.internalsephora.com"
		context.getVariable("defaultSourceSystem") >> "EC"
		context.getVariable(OLRConstants.VARIABLE_ENABLE_LOGGING) >> "true"
		context.getVariable(OLRConstants.VARIABLE_PF_MONITOR) >> "true"
		context.getVariable(OLRConstants.VAR_EC_CENTER_CONN_TIME_OUT) >> "10000"
		context.getVariable(OLRConstants.VAR_EC_CENTER_REC_TIME_OUT) >> "10000"
		context.getVariable(OLRConstants.URI_PATH_ACTIVITY_ID) >> "OLR-EC__cCyZgAck"
		context.getVariable(OLRConstants.VAR_EC_ACCESS_CLIENT_ID) >> "3c075100-5071-42ee-b040-afebae231ad5"
		context.getVariable(OLRConstants.VAR_EC_ACCESS_CLIENT_SECRET) >> "2c4f751380850624db60990b99049849b28db68094b51759794e80a03518315c4aad9081705c777f1e2927c3515dcc44e004900cdf4a656a0f96773b5367e11"
		context.getRequestMessage() >> Mock(Message)
		context.getRequestMessage().getContent() >> inputJsonStringEC
		context.getVariable(_) >> { String key -> getVariable(key)}
		context.setVariable(_, _) >> { String key, Object value -> setVariable(key, value)}
		context.getMessage() >> Mock(Message)
		context.getMessage().setContent(_) >> { String response -> this.response = response}


		when:
		service.execute(context, executionContext)

		then:
		println "\n****************Response**Start***************"
		println context.getVariable(OLRConstants.VAR_TTS_RESPONSE_MESSAGE)
		println "\n****************Response**End*****************"
		//objectMapper.readValue(context.getVariable(OLRConstants.VAR_TTS_RESPONSE_MESSAGE), OLRErrorResponse.class).getStatus() == -4000;
	}
}
