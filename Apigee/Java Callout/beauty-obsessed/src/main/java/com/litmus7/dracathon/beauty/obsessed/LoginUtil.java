/**
 * 
 */
package com.litmus7.dracathon.beauty.obsessed;

import org.apache.commons.lang.StringUtils;

import com.apigee.flow.execution.ExecutionContext;
import com.apigee.flow.execution.ExecutionResult;
import com.apigee.flow.execution.spi.Execution;
import com.apigee.flow.message.MessageContext;

/**
 * @author dinojohnym
 *
 */
public class LoginUtil implements Execution{

	@Override
	public ExecutionResult execute(MessageContext msgContext, ExecutionContext execContext) {
		String loginName = msgContext.getVariable("message.queryparam.loginName");
		msgContext.getMessage().setHeader("Content-Type", "application/json");
		String response ="{}";
		if (StringUtils.isNotEmpty(loginName)) {
			response = "{\n" + 
					"  \"data\": {\n" + 
					"    \"profile\": [\n" + 
					"      {\n" + 
					"        \"name\": \"" + loginName + "\"\n" + 
					"        \n" + 
					"      }\n" + 
					"    ]\n" + 
					"  }\n" + 
					"}";
		}
		msgContext.getMessage().setContent(response);
		return ExecutionResult.SUCCESS;
	}

}
