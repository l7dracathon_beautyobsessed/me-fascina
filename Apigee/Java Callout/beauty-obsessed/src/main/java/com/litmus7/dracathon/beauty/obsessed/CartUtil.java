/**
 * 
 */
package com.litmus7.dracathon.beauty.obsessed;

import org.apache.commons.lang.StringUtils;

import com.apigee.flow.execution.ExecutionContext;
import com.apigee.flow.execution.ExecutionResult;
import com.apigee.flow.execution.spi.Execution;
import com.apigee.flow.message.MessageContext;

/**
 * @author dinojohnym
 *
 */
public class CartUtil implements Execution{
	
	@Override
	public ExecutionResult execute(MessageContext msgContext, ExecutionContext execContext) {
		String loginName = msgContext.getVariable("message.queryparam.loginName");
		msgContext.getMessage().setHeader("Content-Type", "application/json");
		String response ="{}";
		if (StringUtils.isNotEmpty(loginName)) {
			response = "{\n" + 
					"  \"data\": {\n" + 
					"    \"Cart\": [\n" + 
					"      {\n" + 
					"        \"Name\": \"Snow Daze & Snow Nights Frosted Metal Lipstick 3-pc Set\",\n" + 
					"        \"SKU ID\": \"2144608\"\n" + 
					"      },\n" + 
					"      {\n" + 
					"        \"Name\": \"Charlotte's Magic Cream\",\n" + 
					"        \"SKU ID\": \"2117109\"\n" + 
					"      },\n" + 
					"      {\n" + 
					"        \"Name\": \"Tarteguard 30 Vegan Sunscreen Lotion Broad Spectrum SPF 30\",\n" + 
					"        \"SKU ID\": \"1682053\"\n" + 
					"      }\n" + 
					"    ]\n" + 
					"  }\n" + 
					"}";
		} 
		msgContext.getMessage().setContent(response);
		return ExecutionResult.SUCCESS;
	}

}
