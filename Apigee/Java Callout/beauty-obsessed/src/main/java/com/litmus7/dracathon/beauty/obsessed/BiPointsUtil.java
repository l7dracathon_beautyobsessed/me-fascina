/**
 * 
 */
package com.litmus7.dracathon.beauty.obsessed;

import org.apache.commons.lang.StringUtils;

import com.apigee.flow.execution.ExecutionContext;
import com.apigee.flow.execution.ExecutionResult;
import com.apigee.flow.execution.spi.Execution;
import com.apigee.flow.message.MessageContext;

/**
 * @author dinojohnym
 *
 */
public class BiPointsUtil implements Execution{
	
	@Override
	public ExecutionResult execute(MessageContext msgContext, ExecutionContext execContext) {
		String biPoints = msgContext.getVariable("message.queryparam.biPoints");
		msgContext.getMessage().setHeader("Content-Type", "application/json");
		String response ="{}";
		if (StringUtils.isNotEmpty(biPoints)) {
			response = "{\n" + 
					"  \"data\": {\n" + 
					"    \"profile\": [\n" + 
					"      {\n" + 
					"        \"biPoints\": \"" + biPoints + "\"\n" + 
					"        \n" + 
					"      }\n" + 
					"    ]\n" + 
					"  }\n" + 
					"}";
		} 
		msgContext.getMessage().setContent(response);
		return ExecutionResult.SUCCESS;
	}

}
