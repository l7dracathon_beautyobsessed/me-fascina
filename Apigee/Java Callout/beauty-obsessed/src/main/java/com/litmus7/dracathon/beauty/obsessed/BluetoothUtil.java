/**
 * 
 */
package com.litmus7.dracathon.beauty.obsessed;

import org.apache.commons.lang.StringUtils;

import com.apigee.flow.execution.ExecutionContext;
import com.apigee.flow.execution.ExecutionResult;
import com.apigee.flow.execution.spi.Execution;
import com.apigee.flow.message.MessageContext;

/**
 * @author dinojohnym
 *
 */
public class BluetoothUtil implements Execution{

	@Override
	public ExecutionResult execute(MessageContext msgContext, ExecutionContext execContext) {
		String profileId = msgContext.getVariable("message.queryparam.profileId");
		msgContext.getMessage().setHeader("Content-Type", "application/json");
		String response ="{}";
		if (StringUtils.isNotEmpty(profileId)) {
			switch(profileId) {
				case "seph123" : response = "{\n" + 
									"	\"userInfo\" : {\n" + 
									"		\"firstName\": \"Cat\",\n" + 
									"		\"lastName\": \"Lane\",\n" + 
									"		\"imageUrl\": \"https://z2photorankmedia-a.akamaihd.net/media/u/k/s/uks63w3/normal.jpg\",\n" + 
									"		\"biStatus\": \" Rouge\",\n" + 
									"		\"biTraits\": \" dry skin, oily hair\"\n" +
									"	},\n" + 
									"	\"purchaseStatistics\" : { \n" + 
									"		\"Jan\": \"180\",\n" + 
									"		\"Feb\": \"100\",\n" + 
									"		\"Mar\": \"150\",\n" + 
									"		\"Apr\": \"190\",\n" + 
									"		\"May\": \"10\",\n" + 
									"		\"Jun\": \"50\",\n" + 
									"		\"Jul\": \"60\",\n" + 
									"		\"Aug\": \"250\",\n" + 
									"		\"Sep\": \"10\",\n" + 
									"		\"Oct\": \"200\",\n" + 
									"		\"Nov\": \"10\",\n" + 
									"		\"Dec\": \"0\"\n" + 
									"	},\n" + 
									"	\"purchaseHistory\" : [ \n" + 
									"		{\n" + 
									"			\"skuName\":\"BITE BEAUTY\",\n" + 
									"			\"skuImageUrl\":\"https://www.sephora.com/productimages/sku/s2118917-main-Lhero.jpg\"\n" + 
									"		},		\n" + 
									"		{\n" + 
									"			\"skuName\":\"ESTEE LAUDER\",\n" + 
									"			\"skuImageUrl\":\"https://www.sephora.com/productimages/sku/s2112167-main-Lhero.jpg\"\n" + 
									"		}		\n" + 
									"	]	\n" + 
									"}";
								break;
				case "seph456" : response = "{\n" + 
									"	\"userInfo\" : {\n" + 
									"		\"firstName\": \"Akila\",\n" + 
									"		\"lastName\": \"Viswanath\",\n" + 
									"		\"imageUrl\": \"https://photorankmedia-a.akamaihd.net/media/v/j/v/vjv3xd4/normal.jpg\",\n" + 
									"		\"biStatus\": \" VIB\",\n" + 
									"		\"biTraits\": \" oily skin, curly hair\"\n" +
									"	},\n" + 
									"	\"purchaseStatistics\" : { \n" + 
									"		\"Jan\": \"80\",\n" + 
									"		\"Feb\": \"10\",\n" + 
									"		\"Mar\": \"15\",\n" + 
									"		\"Apr\": \"15\",\n" + 
									"		\"May\": \"3\",\n" + 
									"		\"Jun\": \"1\",\n" + 
									"		\"Jul\": \"200\",\n" + 
									"		\"Aug\": \"110\",\n" + 
									"		\"Sep\": \"15\",\n" + 
									"		\"Oct\": \"15\",\n" + 
									"		\"Nov\": \"10\",\n" + 
									"		\"Dec\": \"0\"\n" + 
									"	},\n" + 
									"	\"purchaseHistory\" : [ \n" + 
									"		{\n" + 
									"			\"skuName\":\"MARC JACOBS FRAGRANCES\",\n" + 
									"			\"skuImageUrl\":\"https://www.sephora.com/productimages/sku/s2037034-main-Lhero.jpg\"\n" + 
									"		},		\n" + 
									"		{\n" + 
									"			\"skuName\":\"FENTY BEAUTY BY RIHANNA\",\n" + 
									"			\"skuImageUrl\":\"https://www.sephora.com/productimages/sku/s2018331-main-Lhero.jpg\"\n" + 
									"		},		\n" + 
									"		{\n" + 
									"			\"skuName\":\"MARC JACOBS BEAUTY\",\n" + 
									"			\"skuImageUrl\":\"https://www.sephora.com/productimages/sku/s2098689-main-Lhero.jpg\"\n" + 
									"		}		\n" + 
									"	]	\n" + 
									"}";
									break;
			}
		} 
		msgContext.getMessage().setContent(response);
		return ExecutionResult.SUCCESS;
	}
	
}
